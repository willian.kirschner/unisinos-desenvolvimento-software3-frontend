﻿(function () {
    'use strict';

    angular
        .module('app')
        .component('telaDeErro', {
            templateUrl: 'app/tela-de-erro/tela-de-erro.html',
            controller: ErroController,
            controllerAs: 'vm'
        });

    ErroController.$inject = ['$state'];

    function ErroController($state) {
        var vm = this;
        vm.Title = 'Erro 404';

        vm.RedirectToLogin = function () {
            $state.go('login');
        };
    }

})();
