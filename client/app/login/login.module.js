﻿(function () {
    'use strict';

    angular.module("app.login", [
        'app.common',
        'blockUI'
    ]);

})();