﻿(function () {
    'use strict';

    angular
        .module('app.login')
        .component('login', {
            templateUrl: 'app/login/login.html',
            controller: LoginController,
            controllerAs: 'vm'
        });

    LoginController.$inject = ['CookieHandler', 'LoginDataService', '$location'];

    function LoginController(CookieHandler, LoginDataService, $location) {
        var vm = this;

        vm.ExibeLogin = true;
        vm.ExibeRedefinirSenha = false;
        vm.formLogin = {};

        vm.Logar = logar;
        vm.RedefinirSenha = redefinirSenha;
        vm.IsUsuarioLogado = isUsuarioLogado;
        vm.EnviaEmailRecuperacao = enviaEmailRecuperacao;
        vm.CancelaRedefinirSenha = cancelaRedefinirSenha;

        function logar() {

            var userModel = angular.copy(vm.formLogin);

            //var AES_KEY = userModel.email;
            //userModel.password = CryptoJS.AES.encrypt(userModel.password, AES_KEY).toString();
            //var plaintext = CryptoJS.AES.decrypt(userModel.password, AES_KEY).toString(CryptoJS.enc.Utf8);

            LoginDataService.Logar(userModel)
                .then(function (response) {
                    console.log('Retorno do WS Logar():', response);

                    CookieHandler.set('userId', response.data.id);
                    CookieHandler.set('userData', response.data);

                    $location.path('/chamados');
                }, function (erro) {                    
                    console.log(erro);
                    toastr.error('Dados inválidos', 'Erro ao fazer login');
                });
        };

        function redefinirSenha() {
            vm.emailRecuperacaoSenha = vm.formLogin.email || '';
            vm.ExibeRedefinirSenha = true;
            vm.ExibeLogin = false;
        };

        function isUsuarioLogado() {
            return !!CookieHandler.get('userId');
        };

        function enviaEmailRecuperacao() {
            LoginDataService.RecuperarSenha(vm.emailRecuperacaoSenha)
                .then(function (data) {
                    toastr.success('Siga os passos descritos no e-mail', 'E-mail de redefinição de senha enviado.')
                }, function (erro) {
                    console.log(erro);
                });
        }

        function cancelaRedefinirSenha() {
            vm.ExibeRedefinirSenha = false;
            vm.ExibeLogin = true;
            vm.emailRecuperacaoSenha = '';
        }

    }

})();
;