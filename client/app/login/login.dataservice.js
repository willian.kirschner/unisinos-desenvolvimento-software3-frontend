﻿(function () {
    'use strict';

    angular
        .module("app.login")
        .service("LoginDataService", LoginDataService);

    LoginDataService.$inject = ['$http'];

    function LoginDataService($http) {

        return {
            Logar: logar,
            RecuperarSenha: recuperarSenha
        };

        function logar(user) {
            return $http.post('/api/users/login', user);
        }

        function recuperarSenha(email) {
            return $http.post('/api/login/recoverpassword', email);
        }
    }

})();