﻿(function () {
    'use strict';

    angular.module("app")
           .factory('httpBaseUrlInterceptor', httpBaseUrlInterceptor);

    httpBaseUrlInterceptor.$inject = ['$q', 'URL_BASE_WS', '$location', 'CookieHandler'];
    function httpBaseUrlInterceptor($q, URL_BASE_WS, $location, CookieHandler) {
        return {
            request: function (config) {

                if (!isUrlForApi(config.url)) return config;
                else if (DEBUG) {
                    var parametros = config.data || config.params || '';
                    console.log('Método:', config.method, ' --- URL:', config.url, parametros);
                }

                //temp
                if (config.headers) {
                    config.headers.userId = CookieHandler.get('userId');
                    //console.log('Headers: ', config.headers);
                }

                if (isFullUrlRequest(config.url))
                    return config;

                config.url = URL_BASE_WS + config.url;
                return config;
            },

            responseError: function (rejection) {

                if (DEBUG) return $q.reject(rejection);
                if (!isUrlForApi(rejection.config.url)) return $q.reject(rejection);

                switch (rejection.status) {
                    case 404:
                        console.log('Tentativa de operação não está disponível no momento.');
                        break;
                    case 500:
                        console.log('Erro interno de servidor, tente novamente. Se o erro persistir, contate o administrador do sistema');
                        break;
                    case -1:
                        console.log('Verifique a conexão com a Internet, e tente novamente.');
                        $location.path('/erro');
                        break;
                }
                return $q.reject(rejection);
            }
        };

        function isUrlForApi(url) {
            return url.indexOf('/api/') >= 0;
        }

        function isFullUrlRequest(url) {
            return url.indexOf('http://') >= 0
                || url.indexOf('https://') >= 0;
        }
    }
}());
