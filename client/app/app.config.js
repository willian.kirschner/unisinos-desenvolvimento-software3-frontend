﻿(function () {
    'use strict';

    angular.module("app")
        .config(httpInterceptorConfig)
        .config(ngDialogConfig)
        .config(blockUIConfig)
        .config(localStorageConfig);


    ngDialogConfig.$inject = ['ngDialogProvider'];
    function ngDialogConfig(ngDialogProvider) {
        ngDialogProvider.setDefaults({
            plain: true,
            className: 'ngdialog-theme-plain',
            controllerAs: 'modal',
            closeByDocument: false,
            closeByEscape: true,
            showClose: true,
            preCloseCallback: function (value) {
                if (!value)
                    return true;
                if (value.indexOf('$escape') >= 0)
                    return confirm('Tem certeza que deseja cancelar?');
                return true;
            },
        });

        ngDialogProvider.setOpenOnePerName(true);
    }

    blockUIConfig.$inject = ['blockUIConfig'];
    function blockUIConfig(blockUIConfig) {
        blockUIConfig.message = 'Por favor, aguarde...';
        blockUIConfig.blockBrowserNavigation = true;
        //for $http
        blockUIConfig.autoBlock = true;
    }

    httpInterceptorConfig.$inject = ['$httpProvider'];
    function httpInterceptorConfig($httpProvider) {
        $httpProvider.interceptors.push('httpBaseUrlInterceptor');
    }

    localStorageConfig.$inject = ['localStorageServiceProvider'];
    function localStorageConfig(localStorageServiceProvider) {
        localStorageServiceProvider
            .setPrefix('')
            .setStorageCookie(1, '/', false);
    }

}());
