﻿(function () {
    'use strict';

    angular
        .module('app.users')
        .component('users', {
            templateUrl: 'app/users/users.html',
            controller: UsersController,
            controllerAs: 'vm',
            require: {
                masterPage: '^^masterLayout'
            }
        });

    UsersController.$inject = ['$rootScope', '$scope', '$compile',
        'DTOptionsBuilder', 'DTColumnBuilder', 'DTColumnDefBuilder',
        '$q', 'ngDialog', 'UserDataService'
    ];

    function UsersController(
            $rootScope, $scope, $compile,
            DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder,
            $q, ngDialog, UserDataService
        ) {

        var vm = this;

        vm.$onInit = function () {
            vm.masterPage.TitlePage = "Cadastro de Clientes";
            vm.masterPage.SetaUsuario();
        };

        vm.createUser = createUser;

        activate();

        function activate() {
            vm.dtInstance = {};
            vm.users = {};

            configDataTable();
            registerEvents();
        }

        function createUser() {
            var modal = ngDialog.open({
                name: 'userForm',
                template: '<user-form></user-form>'
            });
        }

        function registerEvents() {
            $rootScope.$on('refreshUsersTable', function (e, data) {
                vm.dtInstance.reloadData();
            });
        }

        function configDataTable() {
            vm.dtOptions = DTOptionsBuilder
                .fromFnPromise(function () {
                    var defer = $q.defer();
                    defer.resolve(UserDataService.ListAll());
                    return defer.promise;
                })
                .withColumnFilter()
                .withPaginationType('simple_numbers')
                .withOption('createdRow', createdRow)
                .withOption('order', [1, 'asc']);

            vm.dtColumns = [
                columnBuilder('id', 'ID', {
                    withClass: 'id-column'
                }),
                columnBuilder('name', 'Cliente', {
                    renderWith: renderTitle
                }),
                columnBuilder(null, 'Ações', {
                    withClass: 'action-column',
                    renderWith: renderRowActions,
                    notSortable: true
                })
            ];
        }

        //columns renderers.
        function columnBuilder(nameColumn, titleColumn, config) {
            var columnBuilder = DTColumnBuilder
                .newColumn(nameColumn)
                .withTitle(titleColumn)
                .withOption('defaultContent', '');

            angular.element('#tableUsers tr.filter-tr')
                .append('<th title="Filtrar por ' + titleColumn.toLowerCase() + '">Filtrar</th>');

            if (!!config) {
                if (config.notVisible)
                    columnBuilder.notVisible();
                if (config.withClass)
                    columnBuilder.withClass(config.withClass);
                if (config.renderWith)
                    columnBuilder.renderWith(config.renderWith);
                if (config.notSortable)
                    columnBuilder.notSortable();
                if (config.defaultContent)
                    columnBuilder.withOption('defaultContent', config.defaultContent);
            }

            return columnBuilder;
        }

        function createdRow(row, data, dataIndex) {
            // Recompiling to bind Angular directives to the DT rows
            $compile(angular.element(row).contents())($scope);
        }
        function renderTitle(data, type, full, meta) {
            return '<b>' + data + '</b>';
        }
        function renderState(data, type, full, meta) {
            var color = 'white';
            var extraFilter = '';
            var title = data + '% funcional';

            switch (data) {
                case '0':
                    data = 'P';
                    title = 'Parado';
                    color = '#f73232';
                    extraFilter = 'Parado';
                    break;
                case '50':
                    color = '#cca35e';
                    break;
                case '75':
                    color = '#e8e81a;';
                    break;
                case '100':
                    color = '#33c333;';
                    break;
            }

            return '<div class="user-state" style="background-color: ' + color + '" title="' + title + '">' +
                       '<span>' + data + '</span>' +
                       '<span class="hide">% ' + extraFilter + '</span>' +
                   '</div>';
        }
        function renderRowActions(data, type, full, meta) {
            vm.users[data.id] = data;
            return '<user-row-actions user="vm.users[' + data.id + ']" />';
        }

    }
})();
