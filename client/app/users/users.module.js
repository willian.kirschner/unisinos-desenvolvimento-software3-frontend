﻿(function () {
    'use strict';

    angular.module("app.users", [
        'datatables',
        'datatables.columnfilter',
        'ngDialog',
        'blockUI',
        'datetime',
        'LocalStorageModule'
    ]);

})();