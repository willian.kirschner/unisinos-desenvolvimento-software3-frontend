﻿(function () {
    'use strict';

    angular
        .module("app.users")
        .service("UserDataService", UserDataService);

    UserDataService.$inject = ['$http', 'DateParserService'];

    function UserDataService($http, DateParserService) {

        return {
            ListAll: listAll,
            Create: create,
            Edit: edit,
            Remove: remove,
            GetUserById: getUserById,
            GetInstitutions: getInstitutions,
            GetTypeUsers: getTypeUsers
        };

        function listAll() {
            return $http.get('/api/users')
                   .then(function (response) {
                       return response.data;
                   }, function (erro) {
                       console.log(erro);
                   });
        }

        function create(user) {
            var model = angular.copy(user);
            return $http.post('/api/users', model);
        }

        function edit(user) {
            var model = angular.copy(user);
            return $http.put('/api/users/' + model.id, model);
        }

        function remove(userId) {
            return $http.delete('/api/users/' + userId);
        }

        function getUserById(id) {
            return $http.get('/api/users/' + id)
                   .then(function (response) {
                       return response.data;
                   }, function (erro) {
                       console.log(erro);
                   });
        }

        function getInstitutions() {
            return $http.get('/api/institutions')
                  .then(function (response) {
                      return response.data;
                  }, function (erro) {
                      console.log(erro);
                  });
        }

        function getTypeUsers() {
            return $http.get('/api/users/typesuser')
                .then(function (response) {
                    return response.data;
                }, function (erro) {
                    console.log(erro);
                });
        }
    }

})();