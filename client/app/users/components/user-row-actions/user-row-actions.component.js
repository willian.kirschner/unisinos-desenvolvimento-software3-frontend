﻿(function () {
    'use strict';

    angular
        .module('app.users')
	    .component('userRowActions', {
	        templateUrl: 'app/users/components/user-row-actions/user-row-actions.html',
	        bindings: {
	            user: "="
	        },
	        controller: RowActionsController,
	        controllerAs: 'vm'
	    });

    RowActionsController.$inject = ['$scope', 'ngDialog', 'UserDataService'];

    function RowActionsController($scope, ngDialog, UserDataService) {
        var vm = this;

        vm.editUser = editUser;
        vm.removeUser = removeUser;

        var _scope = $scope.$new(true);
        _scope.user = vm.user;

        function editUser() {
            ngDialog.open({
                name: 'userForm',
                template: '<user-form user-id="user.id"></user-form>',
                scope: _scope
            });
        }

        function removeUser() {
            if (confirm('Tem certeza que deseja excluir este Cliente?')) {
                UserDataService.Remove(vm.user.id)
                    .then(function (data) {
                        $scope.$emit('refreshUsersTable');
                        toastr.success('Cliente excluído');
                    }, function (erro) {
                        console.log(erro);
                        toastr.error('Erro ao excluir Cliente');
                    });
            }
        }
    }

})();
