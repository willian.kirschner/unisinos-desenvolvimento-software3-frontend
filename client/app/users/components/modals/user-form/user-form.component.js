﻿(function () {
    'use strict';

    angular
    .module('app.users')
    .component('userForm', {
        templateUrl: 'app/users/components/modals/user-form/user-form.html',
        bindings: {
            userId: "=?"
        },
        controller: UserFormController,
        controllerAs: 'vm'
    });

    UserFormController.$inject = [
        '$scope',
        'ngDialog',
        'UserDataService',
        'CookieHandler',
        'DateParserService'
    ];

    function UserFormController($scope, ngDialog, UserDataService, CookieHandler, DateParserService) {
        var vm = this;
        var isEdition = !!vm.userId;

        console.log('UserFormController');

        //var USER_ID = CookieHandler.get('userId');

        vm.TitleModal = isEdition ? "Alteração de cliente" : "Novo cliente";
        vm.SaveUser = saveUser;
        vm.EqualPasswords = equalPasswords;

        activate();

        function activate() {
            vm.user = {};

            populateFields();
            populateOptions();
        }

        function populateFields() {
            if (isEdition) {
                UserDataService.GetUserById(vm.userId)
                    .then(function (data) {
                        vm.user = data;
                        console.log('vm.user', vm.user);
                    }, function (erro) {
                        console.log(erro);
                    });
            }
        }

        function populateOptions() {
            //http://jsfiddle.net/JB3Un/
            //vm.institutions = [{
            //    id: 0,
            //    description: "Carregando instituições. Por favor, aguarde um instante..."
            //}];

            vm.institutions = [];

            UserDataService.GetInstitutions()
                .then(function (data) {
                    vm.institutions = data;
                }, function (erro) {
                    console.log(erro);
                });

            vm.permissionLevels = [{
                id: 0,
                description: 'Super Administrador'
            }, {
                id: 1,
                description: 'Administrador'
            }, {
                id: 2,
                description: 'Usuário normal'
            }, {
                id: 3,
                description: 'Usuário visualizador'
            }];
        }

        function saveUser() {
            console.log('vm.user', vm.user);

            var promise = isEdition
                ? UserDataService.Edit(vm.user)
                : UserDataService.Create(vm.user);

            promise.then(function () {
                $scope.$emit('refreshUsersTable');
                toastr.success('Cliente salvo com sucesso');
                ngDialog.closeAll();
            }, function (erro) {
                console.log(erro);
                var msgErro = !!erro.data ? erro.data.message : '';
                toastr.error(msgErro, 'Erro ao salvar Cliente');
            });


        }

        function equalPasswords() {
            if (!vm.user.password && !vm.user.repeatPassword)
                return true;

            var it = vm.user.password == vm.user.repeatPassword;
            return it;
        }
    }

})();
