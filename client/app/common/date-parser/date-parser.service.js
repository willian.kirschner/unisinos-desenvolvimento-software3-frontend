﻿(function () {
    'use strict';

    angular
        .module("app.tickets")
        .service("DateParserService", DateParserService);

    DateParserService.$inject = ['datetime'];

    function DateParserService(datetime) {

        var _format = "dd/MM/yyyy HH:mm";

        return {
            DateToString: dateToString,
            StringToDate: stringToDate,
            GetTodayDate: getTodayDate,
            //NormalizeDate: normalizeDate,
            GetParserDate: datetime(_format)
        };

        function dateToString(date) {
            if (!date) return '';
            return datetime(_format).setDate(date).getText();
        }

        function stringToDate(text) {
            return datetime(_format).parse(text).getDate();
        }

        function getTodayDate() {
            return datetime(_format).setDate(new Date()).getDate();
        }

        //function normalizeDate(date) {
        //    if (typeof (date) == "string") {
        //        date = stringToDate(date);                
        //    }
        //    return date;
        //}

    }

})();