﻿(function () {
    'use strict';

    angular
        .module("app.common")
        .service("CookieHandler", CookieHandler);

    CookieHandler.$inject = ['localStorageService'];

    function CookieHandler(localStorageService) {
        return localStorageService.cookie;
    }

})();