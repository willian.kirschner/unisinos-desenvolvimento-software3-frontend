﻿(function () {
    'use strict';

    angular.module("app.common", [
        'datetime',
        'LocalStorageModule'
    ]);

})();