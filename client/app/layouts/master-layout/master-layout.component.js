﻿(function () {
    'use strict';

    angular
    .module('app.layouts')
    .component('masterLayout', {
        templateUrl: 'app/layouts/master-layout/master-layout.html',
        controller: MasterLayoutController,
        controllerAs: 'vm'
    });

    MasterLayoutController.$inject = ['CookieHandler'];

    function MasterLayoutController(CookieHandler) {
        var vm = this;

        vm.Logout = logout;
        vm.SetaUsuario = setaUsuario;

        activate();

        function activate() { }

        function logout() {
            CookieHandler.remove('userId');
            CookieHandler.remove('userData');
        }

        function setaUsuario() {
            var userData = CookieHandler.get('userData');
            vm.NameUser = "Olá, " + userData.name;
        }
    }
})();
