﻿(function () {
    'use strict';

    angular.module('app', [
        //common use
        'ui.router',

        //app
        'app.layouts',
        'app.login',
        'app.tickets',
        'app.users'
    ]);
}());
