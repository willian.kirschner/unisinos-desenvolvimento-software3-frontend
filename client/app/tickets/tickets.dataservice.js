﻿(function () {
    'use strict';

    angular
        .module("app.tickets")
        .service("TicketDataService", TicketDataService);

    TicketDataService.$inject = ['$http', 'DateParserService'];

    function TicketDataService($http, DateParserService) {

        return {
            ListAll: listAll,
            Create: create,
            Edit: edit,
            Close: close,
            Remove: remove,

            GetTicketById: getTicketById,
            GetEquipments: getEquipments,
            GetUsers: getUsers,
            GetInstitutions: getInstitutions
        };

        function listAll() {
            return $http.get('/api/tickets')
                   .then(function (response) {
                       return response.data;
                   }, function (erro) {
                       console.log(erro);
                   });
        }

        function create(ticket) {
            var model = angular.copy(ticket);
            model.dateOcurrence = DateParserService.DateToString(model.dateOcurrence);
            model.prediction = DateParserService.DateToString(model.prediction);

            return $http.post('/api/tickets', model);
        }

        function edit(ticket) {
            var model = angular.copy(ticket);
            model.dateOcurrence = DateParserService.DateToString(model.dateOcurrence);
            model.prediction = DateParserService.DateToString(model.prediction);

            return $http.put('/api/tickets/' + model.id, model);
        }

        function close(ticket) {
            var model = angular.copy(ticket);
            model.state = 'f';
            model.dateClosing = DateParserService.DateToString(model.dateClosing);

            return $http.put('/api/tickets/' + model.id + '/close', model);
        }

        function remove(ticket) {
            var model = angular.copy(ticket);
            model.state = 'e';
            model.dateRemoving = DateParserService.DateToString(model.dateRemoving);

            return $http.put('/api/tickets/' + model.id + '/delete', model);
        }

        function getEquipments(institutionId) {
            return $http.get('/api/institutions/' + institutionId + '/equipments?fields=id,name')
                   .then(function (response) {
                       return response.data;
                   }, function (erro) {
                       console.log(erro);
                   });
        }

        function getUsers() {
            return $http.get('/api/users')
                   .then(function (response) {
                       return response.data;
                   }, function (erro) {
                       console.log(erro);
                   });
        }

        function getTicketById(id) {
            return $http.get('/api/tickets/' + id)
                   .then(function (response) {
                       return response.data;
                   }, function (erro) {
                       console.log(erro);
                   });
        }

        function getInstitutions() {
            return $http.get('/api/institutions')
                  .then(function (response) {
                      return response.data;
                  }, function (erro) {
                      console.log(erro);
                  });
        }
    }

})();