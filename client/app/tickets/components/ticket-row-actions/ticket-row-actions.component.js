﻿(function () {
    'use strict';

    angular
        .module('app.tickets')
	    .component('ticketRowActions', {
	        templateUrl: 'app/tickets/components/ticket-row-actions/ticket-row-actions.html',
	        bindings: {
	            ticket: "="
	        },
	        controller: RowActionsController,
	        controllerAs: 'vm'
	    });

    RowActionsController.$inject = ['$scope', 'ngDialog', 'TicketDataService'];

    function RowActionsController($scope, ngDialog, TicketDataService) {
        var vm = this;

        vm.Edit = edit;
        vm.Close = close;
        vm.Remove = remove;
        vm.OpenNotes = openNotes;
        vm.OpenCosts = openCosts;

        var _scope = $scope.$new(true);
        _scope.ticket = vm.ticket;

        function edit() {
            ngDialog.open({
                name: 'ticketForm',
                template: '<ticket-form ticket-id="ticket.id"></ticket-form>',
                scope: _scope
            });
        }

        function close() {
            ngDialog.open({
                name: 'ticketClose',
                template: '<ticket-close ticket="ticket" type="C" />',
                scope: _scope
            });
        }

        function remove() {
            ngDialog.open({
                name: 'ticketClose',
                template: '<ticket-close ticket="ticket" type="R" />',
                scope: _scope
            });
            //if (confirm('Tem certeza que deseja excluir este chamado?')) {
            //    TicketDataService.Remove(vm.ticket.id)
            //        .then(function (data) {
            //            toastr.success('Chamado excluído');
            //            $scope.$emit('refreshTicketsTable');
            //        }, function (data) {
            //            console.log(data);
            //        });
            //}
        }

        function openNotes() {
            ngDialog.open({
                name: 'ticketNotes',
                template: '<ticket-notes ticket="ticket" />',
                scope: _scope
            });
        }

        function openCosts() {
            ngDialog.open({
                name: 'ticketCosts',
                template: '<ticket-costs ticket="ticket"></ticket-costs>',
                scope: _scope
            });
        }
    }

})();