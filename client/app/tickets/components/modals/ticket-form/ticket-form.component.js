﻿(function () {
    'use strict';

    angular
    .module('app.tickets')
    .component('ticketForm', {
        templateUrl: 'app/tickets/components/modals/ticket-form/ticket-form.html',
        bindings: {
            ticketId: "=?"
        },
        controller: TicketFormController,
        controllerAs: 'vm'
    });

    TicketFormController.$inject = [
        '$scope',
        'ngDialog',
        'TicketDataService',
        'DateParserService',
        'CookieHandler'
    ];

    function TicketFormController($scope, ngDialog, TicketDataService, DateParserService, CookieHandler) {
        var vm = this;
        var isEdition = !!vm.ticketId;

        //var USER_ID = CookieHandler.get('userId');

        vm.TitleModal = isEdition ? "Alteração de Ticket" : "Abertura de Ticket";
        vm.ClosedOrRemoved = false;

        vm.ChangeInstitution = changeInstitution;
        vm.SaveTicket = saveTicket;

        activate();

        function activate() {
            populateFields();
            populateOptions();
        }

        function populateFields() {
            if (isEdition) {
                TicketDataService.GetTicketById(vm.ticketId)
                    .then(function (data) {
                        vm.ticket = data;
                        vm.ticket.dateOcurrence = DateParserService.StringToDate(vm.ticket.dateOcurrence);
                        vm.ticket.prediction = DateParserService.StringToDate(vm.ticket.prediction);

                        if (vm.ticket.institutionId) getEquipments(vm.ticket.institutionId);
                        if (vm.ticket.state != 'a') {
                            vm.TitleModal = "Dados do Ticket";
                            vm.ClosedOrRemoved = true;
                        }
                    }, function (erro) {
                        console.log(erro);
                    });
            }
            else {
                //Default values
                vm.ticket = {};
                vm.ticket.situation = '50';
                vm.ticket.priority = 'b';
                vm.ticket.dateOcurrence = DateParserService.GetTodayDate();
                vm.ticket.prediction = DateParserService.GetTodayDate();
            }
        }

        function populateOptions() {
            getInstitutions();
            getUsers();
        }

        function getInstitutions() {
            TicketDataService.GetInstitutions()
                .then(function (data) {
                    vm.Institutions = data;
                }, function (erro) {
                    console.log(erro);
                });
        }

        function getEquipments(institutionId) {
            TicketDataService.GetEquipments(institutionId)
                .then(function (data) {
                    vm.Equipments = data;
                }, function (erro) {
                    console.log(erro);
                });
        }

        function getUsers() {
            TicketDataService.GetUsers()
                .then(function (data) {
                    vm.Responsables = data;
                }, function (erro) {
                    console.log(erro);
                });
        }

        function saveTicket() {

            //vm.ticket.userId = USER_ID;

            var promise = isEdition
                ? TicketDataService.Edit(vm.ticket)
                : TicketDataService.Create(vm.ticket);

            promise.then(function () {
                $scope.$emit('refreshTicketsTable');
                toastr.success('Ticket salvo com sucesso');
                ngDialog.closeAll();
            }, function (erro) {
                console.log(erro);
                var msgErro = !!erro.data ? erro.data.message : '';
                toastr.error(msgErro, 'Erro ao salvar Ticket');
            });

        };

        function changeInstitution(institutionId) {
            getEquipments(institutionId);
        }
    }

})();
