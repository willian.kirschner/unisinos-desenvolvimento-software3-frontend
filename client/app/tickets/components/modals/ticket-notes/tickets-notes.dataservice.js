﻿(function () {
    'use strict';

    angular
        .module("app.tickets")
        .service("TicketNotesDataService", TicketNotesDataService);

    TicketNotesDataService.$inject = ['$http'];

    function TicketNotesDataService($http) {

        return {
            NotesHandler: notesHandler
        }

        function notesHandler(ticketId) {

            return {
                ListAll: listAll,
                SaveNote: saveNote,
                Remove: remove
            };

            function listAll() {
                return $http.get('/api/tickets/' + ticketId + '/notes')
                       .then(function (response) {
                           console.log(response.data);
                           return response.data;
                       }, function (erro) {
                           console.log(erro);
                       });
            }

            function saveNote(note) {
                var promise = note.id
                    ? $http.put('/api/tickets/' + ticketId + '/notes/' + note.id, note)
                    : $http.post('/api/tickets/' + ticketId + '/notes', note);

                return promise.then(function (response) {
                    return response.data;
                }, function (erro) {
                    console.log(erro);
                });
            }

            function remove(noteId) {
                return $http.delete('/api/notes/' + noteId);
            }
        }
    }

})();