﻿(function () {
    'use strict';

    angular
        .module('app.tickets')
	    .component('ticketNotes', {
	        templateUrl: 'app/tickets/components/modals/ticket-notes/ticket-notes.html',
	        controller: NotesController,
	        controllerAs: 'vm',
	        bindings: {
	            ticket: "="
	        }
	    });

    NotesController.$inject = [
        '$scope',
        '$compile',
        '$q',
        'TicketNotesDataService',
        'DTOptionsBuilder',
        'DTColumnBuilder',
        'CookieHandler'
    ];

    function NotesController($scope, $compile, $q, TicketNotesDataService,
        DTOptionsBuilder, DTColumnBuilder, CookieHandler) {
        var vm = this;

        var USER_ID = CookieHandler.get('userId');

        vm.TitleModal = vm.ticket.title;
        vm.AllNotes = [];
        vm.dtInstance = {};

        vm.AddNote = addNote;
        vm.SaveNote = saveNote;
        vm.EditNote = editNote;
        vm.RemoveNote = removeNote;
        vm.CancelNote = cancelNote;

        var NotesDataService = TicketNotesDataService.NotesHandler(vm.ticket.id);

        activate();

        function activate() {
            configDataTable();
            registerEvents();
        }

        function configDataTable() {
            vm.dtOptions = DTOptionsBuilder
            .fromFnPromise(function () {
                var defer = $q.defer();
                defer.resolve(NotesDataService.ListAll());
                return defer.promise;
            })
            .withOption('createdRow', createdRow)
            .withPaginationType('simple_numbers')
            .withOption('order', [3, 'asc']);

            vm.dtColumns = [
                columnBuilder('id', 'ID', {
                    withClass: 'id-column'
                }),
                columnBuilder('description', 'Nota'),
                columnBuilder('user', 'Usuário'),
                columnBuilder('date', 'Data'),
                columnBuilder(null, 'Ações', {
                    withClass: 'action-column',
                    renderWith: renderRowActions,
                    notSortable: true,
                    notVisible: (vm.ticket.state != 'a')
                })
            ];
        }

        function registerEvents() {
            $scope.$on('refreshNotesTable', function (e, data) {
                //console.log('refreshNotesTable');
                vm.dtInstance.reloadData();
            });
        }

        function addNote() {
            vm.Note = {
                //userId: USER_ID
            };
            vm.showAddNote = true;
        }

        function saveNote() {
            NotesDataService.SaveNote(vm.Note)
                .then(function (data) {
                    toastr.success('Nota salva');
                    $scope.$broadcast('refreshNotesTable');
                    vm.Note = {};
                    vm.showAddNote = false;
                }, function (erro) {
                    toastr.error('Erro ao salvar nota');
                    console.log(erro);
                });
        }

        function editNote(noteId) {
            vm.Note = vm.AllNotes[noteId];
            //vm.Note.userId = USER_ID;
            vm.showAddNote = true;
        }

        function removeNote(noteId) {
            if (confirm('Tem certeza que deseja excluir esta nota?')) {
                NotesDataService.Remove(noteId)
                    .then(function (data) {
                        $scope.$broadcast('refreshNotesTable');
                    }, function (erro) {
                        toastr.error('Erro ao excluir nota');
                        console.log(erro);
                    });
            }
        }

        function cancelNote() {
            vm.showAddNote = false;
            vm.Note.description = '';
        }

        function renderRowActions(data, type, full, meta) {
            vm.AllNotes[data.id] = data;

            //Usuários podem editar/excluir apenas as próprias notas
            if (data.userId != USER_ID)
                return;

            var btnEdit =
                "<button ng-click='vm.EditNote(" + data.id + ")' class='btn btn-warning' title='Editar nota'>" +
                    "<i class='fa fa-edit'></i>" +
                "</button>";

            var btnRemove =
                "<button ng-click='vm.RemoveNote(" + data.id + ")' class='btn btn-danger' title='Excluir nota'>" +
                    "<i class='fa fa-trash'></i>" +
                "</button>";

            return "<div class='ticket-modal-row'>" + btnEdit + btnRemove + "</div>";
        }

        function createdRow(row, data, dataIndex) {
            // Recompiling to bind Angular directives to the DT rows
            $compile(angular.element(row).contents())($scope);
        }

        function columnBuilder(nameColumn, titleColumn, config) {
            var columnBuilder = DTColumnBuilder
                .newColumn(nameColumn)
                .withTitle(titleColumn)
                .withOption('defaultContent', '');

            if (!!config) {
                if (config.notVisible)
                    columnBuilder.notVisible();
                if (config.withClass)
                    columnBuilder.withClass(config.withClass);
                if (config.renderWith)
                    columnBuilder.renderWith(config.renderWith);
                if (config.notSortable)
                    columnBuilder.notSortable();
                if (config.defaultContent)
                    columnBuilder.withOption('defaultContent', config.defaultContent);
            }

            return columnBuilder;
        }
    }

})();