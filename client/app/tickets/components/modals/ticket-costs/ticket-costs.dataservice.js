﻿(function () {
    'use strict';

    angular
        .module("app.tickets")
        .service("TicketCostsDataService", TicketCostsDataService);

    TicketCostsDataService.$inject = ['$http', 'DateParserService', '$q'];

    function TicketCostsDataService($http, DateParserService, $q) {

        return {
            CostsHandler: costsHandler
        }

        function costsHandler(ticketId) {

            return {
                ListAll: listAll,
                SaveCost: saveCost,
                Remove: remove
            };

            function listAll() {
                return $http.get('/api/tickets/' + ticketId + '/costs')
                       .then(function (response) {
                           return response.data;
                       }, function (erro) {
                           console.log(erro);
                       });
            }

            function saveCost(cost) {
                var model = angular.copy(cost);

                var promise = model.id
                    ? $http.put('/api/tickets/' + ticketId + '/costs/' + model.id, model)
                    : $http.post('/api/tickets/' + ticketId + '/costs', model);

                var deferred = $q.defer();

                promise.then(function (response) {
                    console.log(response);
                    deferred.resolve(response.data);
                }, function (erro) {
                    deferred.reject(erro);
                });

                return deferred.promise;
            }

            function remove(costId) {
                return $http.delete('/api/costs/' + costId);
            }
        }
    }

})();