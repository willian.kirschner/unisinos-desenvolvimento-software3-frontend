﻿(function () {
    'use strict';

    angular
        .module('app.tickets')
	    .component('ticketCosts', {
	        templateUrl: 'app/tickets/components/modals/ticket-costs/ticket-costs.html',
	        controller: CostsController,
	        controllerAs: 'vm',
	        bindings: {
	            ticket: "="
	        }
	    });

    CostsController.$inject = [
        '$scope',
        '$compile',
        '$q',
        'TicketCostsDataService',
        'DTOptionsBuilder',
        'DTColumnBuilder',
        'CookieHandler'
    ];

    function CostsController($scope, $compile, $q, TicketCostsDataService,
        DTOptionsBuilder, DTColumnBuilder, CookieHandler) {
        var vm = this;

        var USER_ID = CookieHandler.get('userId');

        vm.TitleModal = vm.ticket.title;
        vm.AllCosts = [];
        vm.dtInstance = {};

        vm.AddCost = addCost;
        vm.SaveCost = saveCost;
        vm.EditCost = editCost;
        vm.RemoveCost = removeCost;
        vm.ClearAddCost = clearAddCost;

        var CostsDataService = TicketCostsDataService.CostsHandler(vm.ticket.id);

        activate();

        function activate() {
            configDataTable();
            registerEvents();
        }

        function configDataTable() {
            vm.dtOptions = DTOptionsBuilder
            .fromFnPromise(function () {
                var defer = $q.defer();
                defer.resolve(CostsDataService.ListAll());
                return defer.promise;
            })
            .withOption('createdRow', createdRow)
            .withPaginationType('simple_numbers')
            .withOption('order', [3, 'asc']);

            vm.dtColumns = [
                columnBuilder('id', 'ID', {
                    withClass: 'id-column'
                }),
                columnBuilder('value', 'Custo'),
                columnBuilder('description', 'Descrição'),
                columnBuilder('user', 'Usuário'),
                columnBuilder(null, 'Ações', {
                    withClass: 'action-column',
                    renderWith: renderRowActions,
                    notSortable: true,
                    notVisible: (vm.ticket.state != 'a')
                })
            ];
        }

        function registerEvents() {
            $scope.$on('refreshCostsTable', function (e, data) {
                //console.log('refreshCostsTable');
                vm.dtInstance.reloadData();
            });
        }

        function addCost() {
            vm.Cost = {
                //userId: USER_ID
            };

            vm.showAddCost = true;
        }

        function saveCost() {
            CostsDataService.SaveCost(vm.Cost)
                .then(function (data) {
                    console.log(data);
                    toastr.success('Custo salvo');
                    $scope.$broadcast('refreshCostsTable');
                    vm.Cost = {};
                    vm.showAddCost = false;
                }, function (erro) {
                    toastr.error(erro.data.message, 'Erro ao salvar custo');
                    console.log(erro);
                });
        }

        function editCost(costId) {
            vm.Cost = vm.AllCosts[costId];
            //vm.Cost.userId = USER_ID;

            vm.showAddCost = true;
        }

        function removeCost(costId) {
            if (confirm('Tem certeza que deseja excluir este custo?')) {
                CostsDataService.Remove(costId)
                    .then(function (data) {
                        $scope.$broadcast('refreshCostsTable');
                    }, function (erro) {
                        toastr.error(erro.data.message, 'Erro ao salvar custo');
                        console.log(erro);
                    });
            }
        }

        function clearAddCost() {
            vm.showAddCost = false;
            vm.Cost = {};
        }

        function renderRowActions(data, type, full, meta) {

            vm.AllCosts[data.id] = data;

            //Usuários podem editar/excluir apenas os próprios custos
            if (data.userId != USER_ID)
                return;

            var btnEdit =
                "<button ng-click='vm.EditCost(" + data.id + ")' class='btn btn-warning' title='Editar custo'>" +
                    "<i class='fa fa-edit'></i>" +
                "</button>";

            var btnRemove =
                "<button ng-click='vm.RemoveCost(" + data.id + ")' class='btn btn-danger' title='Excluir custo'>" +
                    "<i class='fa fa-trash'></i>" +
                "</button>";

            return "<div class='ticket-modal-row'>" + btnEdit + btnRemove + "</div>";
        }

        function createdRow(row, data, dataIndex) {
            // Recompiling to bind Angular directives to the DT rows
            $compile(angular.element(row).contents())($scope);
        }

        function columnBuilder(nameColumn, titleColumn, config) {

            var columnBuilder = DTColumnBuilder
                .newColumn(nameColumn)
                .withTitle(titleColumn)
                .withOption('defaultContent', '');

            if (!!config) {
                if (config.notVisible)
                    columnBuilder.notVisible();
                if (config.withClass)
                    columnBuilder.withClass(config.withClass);
                if (config.renderWith)
                    columnBuilder.renderWith(config.renderWith);
                if (config.notSortable)
                    columnBuilder.notSortable();
                if (config.defaultContent)
                    columnBuilder.withOption('defaultContent', config.defaultContent);
            }

            return columnBuilder;
        }
    }

})();