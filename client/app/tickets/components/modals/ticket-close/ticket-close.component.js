﻿(function () {
    'use strict';

    angular
        .module('app.tickets')
	    .component('ticketClose', {
	        templateUrl: 'app/tickets/components/modals/ticket-close/ticket-close.html',
	        bindings: {
	            ticket: "<",
	            type: "@"
	        },
	        controller: CloseTicketController,
	        controllerAs: 'vm'
	    });

    CloseTicketController.$inject = ['$scope', 'TicketDataService', 'ngDialog', 'CookieHandler'];

    function CloseTicketController($scope, TicketDataService, ngDialog, CookieHandler) {
        var vm = this;

        //var USER_ID = CookieHandler.get('userId');

        vm.CloseTicket = closeTicket;

        activate();

        function activate() {
            switch (vm.type) {
                case 'C': //Fechamento
                    vm.TitleModal = 'Fechamento do Ticket';
                    vm.TitleNote = 'Nota de fechamento';
                    vm.BtnTitleNote = 'Fechar Ticket';
                    break;
                case 'R': //Excluir	
                    vm.TitleModal = 'Exclusão de Ticket';
                    vm.TitleNote = 'Nota de exclusão';
                    vm.BtnTitleNote = 'Excluir Ticket';
                    break;
            }
        }

        function closeTicket() {

            //vm.ticket.userId = USER_ID;

            switch (vm.type) {
                case 'C': //Fechamento
                    if (confirm('Tem certeza que deseja fechar este Ticket?')) {
                        TicketDataService.Close(vm.ticket)
                            .then(function (data) {
                                $scope.$emit('refreshTicketsTable');
                                toastr.success('Ticket fechado');
                                ngDialog.closeAll();
                            }, function (data) {
                                toastr.error('Erro ao fechar Ticket');
                                console.log(data);
                            });
                    }
                    break;
                case 'R': //Excluir
                    if (confirm('Tem certeza que deseja excluir este Ticket?')) {
                        TicketDataService.Remove(vm.ticket)
                            .then(function (data) {
                                $scope.$emit('refreshTicketsTable');
                                toastr.success('Ticket excluído');
                                ngDialog.closeAll();
                            }, function (data) {
                                toastr.error('Erro ao excluir Ticket');
                                console.log(data);
                            });
                    }
                    break;
            }
        }
    }

})();
