﻿(function () {
    'use strict';

    angular
        .module('app.tickets')
        .component('tickets', {
            templateUrl: 'app/tickets/tickets.html',
            controller: TicketsController,
            controllerAs: 'vm',
            require: {
                masterPage: '^^masterLayout'
            }
        });

    TicketsController.$inject = [
        '$rootScope',
        '$scope',
        '$compile',
        'DTOptionsBuilder',
        'DTColumnBuilder',
        'DTColumnDefBuilder',
        '$q',
        'ngDialog',
        'TicketDataService'
    ];

    function TicketsController(
            $rootScope, $scope, $compile,
            DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder,
            $q, ngDialog, TicketDataService
        ) {

        var vm = this;

        vm.$onInit = function () {
            vm.masterPage.TitlePage = "Controle de Estacionamento - Cadastro de Tickets";
            vm.masterPage.SetaUsuario();
        };

        vm.dtInstance = {};
        vm.tickets = {};

        vm.OpenNewTicket = openNewTicket;

        activate();

        function activate() {
            configDataTable();
            registerEvents();
        }

        function openNewTicket() {
            var modal = ngDialog.open({
                name: 'ticketForm',
                template: '<ticket-form></ticket-form>'
            });
        }

        function registerEvents() {
            $rootScope.$on('refreshTicketsTable', function (e, data) {
                //console.log('refreshTicketsTable');
                vm.dtInstance.reloadData();
            });
        }

        function configDataTable() {
            vm.dtOptions = DTOptionsBuilder
                .fromFnPromise(function () {
                    var defer = $q.defer();
                    defer.resolve(TicketDataService.ListAll());
                    return defer.promise;
                })
                .withColumnFilter()
                .withPaginationType('simple_numbers')
                .withOption('createdRow', createdRow)
                .withOption('order', [
                    [1, 'asc'], //Estado
                    [10, 'asc'], //Prioridade
                    [3, 'asc'] //Instituição
                ]);

            vm.dtColumns = [
                columnBuilder('id', 'ID', {
                    withClass: 'id-column'
                }),
                columnBuilder('state', 'Estado', {
                    withClass: 'state-column',
                    renderWith: renderState
                }),
                columnBuilder('title', 'Placa', {
                    renderWith: renderTitle
                }),
                columnBuilder('institution', 'Tipo de Veículo', {
                    withClass: 'institution-column',
                    renderWith: renderInstitution
                }),
                columnBuilder('equipment', 'Equipamento'),
                columnBuilder('dateOcurrence', 'Data de Entrada'),
                columnBuilder('prediction', 'Previsão de Saída'),
                columnBuilder('openedBy', 'Aberto por'),
                columnBuilder('responsable', 'Cliente'),
                columnBuilder('situation', 'Situação', {
                    withClass: 'situation-column',
                    renderWith: renderSituation
                }),
                columnBuilder('priority', 'Prioridade', {
                    renderWith: renderPriority
                }),
                columnBuilder(null, 'Ações', {
                    withClass: 'action-column',
                    renderWith: renderRowActions,
                    notSortable: true
                })
            ];
        }

        //columns renderers.
        function columnBuilder(nameColumn, titleColumn, config) {
            var columnBuilder = DTColumnBuilder
                .newColumn(nameColumn)
                .withTitle(titleColumn)
                .withOption('defaultContent', '');

            angular.element('#tableTickets tr.filter-tr')
                .append('<th title="Filtrar por ' + titleColumn.toLowerCase() + '">Filtrar</th>');

            if (!!config) {
                if (config.notVisible)
                    columnBuilder.notVisible();
                if (config.withClass)
                    columnBuilder.withClass(config.withClass);
                if (config.renderWith)
                    columnBuilder.renderWith(config.renderWith);
                if (config.notSortable)
                    columnBuilder.notSortable();
                if (config.defaultContent)
                    columnBuilder.withOption('defaultContent', config.defaultContent);
            }

            return columnBuilder;
        }

        function createdRow(row, data, dataIndex) {
            // Recompiling to bind Angular directives to the DT rows
            $compile(angular.element(row).contents())($scope);
        }
        function renderTitle(data, type, full, meta) {
            return '<b>' + data + '</b>';
        }
        function renderState(data, type, full, meta) {
            var html = '';

            switch (data) {
                case 'a': return '<span class="fa fa-wrench ticket-state" title="Chamado em aberto"><span class="hide">1 em aberto / progresso</span></span>';
                case 'f': return '<span class="fa fa-check-circle ticket-state" title="Chamado fechado"><span class="hide">2 finalizado / fechado</span></span>';
                case 'e': return '<span class="fa fa-close ticket-state" title="Chamado excluído"><span class="hide">3 excluído</span></span>';
                default: return '';
            }
        }
        function renderSituation(data, type, full, meta) {
            var color = 'white';
            var extraFilter = '';
            var title = data + '% funcional';

            switch (data) {
                case '0':
                    data = 'P';
                    title = 'Parado';
                    color = '#f73232';
                    extraFilter = 'Parado';
                    break;
                case '50':
                    color = '#cca35e';
                    break;
                case '75':
                    color = '#e8e81a;';
                    break;
                case '100':
                    color = '#33c333;';
                    break;
            }

            return '<div class="ticket-situation" style="background-color: ' + color + '" title="' + title + '">' +
                       '<span>' + data + '</span>' +
                       '<span class="hide">% ' + extraFilter + '</span>' +
                   '</div>';
        }
        function renderPriority(data, type, full, meta) {
            switch (data) {
                case 'a': return '<span style="display: none;">1</span><span class="field-required">Alta</span>';
                case 'n': return '<span style="display: none;">2</span>Normal';
                case 'b': return '<span style="display: none;">3</span><span style="color: blue">Baixa</span>';
                default: return '';
            }
        }
        function renderInstitution(data, type, full, meta) {
            var imgPath = 'content/images/institutions/';
            switch (data) {
                case 'Menino Deus':
                    imgPath += '1.png';
                    break;
                case 'Hospital de Clínicas (POA)':
                    imgPath += '2.png';
                    break;
                default:
                    data = "NomeHospital"
                    imgPath += '1.png';
                    break;
            }

            return '<span style="display: none;">' + data + '</span>' +
                   '<img width="30" height="30" title="' + data + '" src="' + imgPath + '" />';
        }
        function renderRowActions(data, type, full, meta) {
            vm.tickets[data.id] = data;
            return '<ticket-row-actions ticket="vm.tickets[' + data.id + ']" />';
        }
    }

})();
