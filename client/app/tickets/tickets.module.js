﻿(function () {
    'use strict';

    angular.module("app.tickets", [
        'datatables',
        'datatables.columnfilter',
        'ngDialog',
        'blockUI',
        'datetime',
        'LocalStorageModule'
    ]);

})();