﻿(function () {
    'use strict';

    angular
        .module("app")
        .run(dtLanguageConfig)
        .run(routeConfig);

    dtLanguageConfig.$inject = ['DTDefaultOptions'];
    function dtLanguageConfig(DTDefaultOptions) {
        var ptbr = 'app/common/datatables-Portuguese.json';

        DTDefaultOptions
            .setDisplayLength(10)
            .setLanguageSource(ptbr)
            .setLanguage({
                sUrl: ptbr
            });
    }

    routeConfig.$inject = ['$rootScope', '$location', 'CookieHandler'];
    function routeConfig($rootScope, $location, CookieHandler) {
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

            //console.log('$stateChangeStart >> userId:', CookieHandler.get('userId'));
            //console.log(toState);

            if (toState.name == "login")
                return;

            if (!CookieHandler.get('userId')) {
                event.preventDefault();
                $location.path('/login');
            }
        });
    }
})();