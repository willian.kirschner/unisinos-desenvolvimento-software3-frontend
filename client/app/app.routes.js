﻿
(function () {
    'use strict';

    angular.module('app')
        .config(configRoutes);

    configRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

    function configRoutes($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise("/login");

        $stateProvider
            .state('login', {
                url: "/login",
                template: "<login></login>"
            })
            .state('erro', {
                url: "/erro",
                template: "<tela-de-erro></tela-de-erro>"
            });

        $stateProvider
            .state('master', {
                template: "<master-layout></master-layout>"
            })
                .state('master.usuarios', {
                    url: "/usuarios",
                    template: "<users></users>"
                })
                .state('master.chamados', {
                    url: "/chamados",
                    template: "<tickets></tickets>"
                });
    }
}());
